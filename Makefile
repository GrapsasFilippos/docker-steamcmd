##
 # Copyright (c) <2017> <Filippos A. Grapsas> public001@grapsas.com
 # MIT License
 #
 # Crated at 22/09/2018
 ##

DIR := ${CURDIR}

DI_NAME := $(or $(DI_NAME),graphi/steamcmd)
DI_TAG := $(or $(DI_TAG),0.0.0)
DC_NAME := $(or $(DC_NAME),l.$(shell whoami).steamcmd)

DI_ID := $(shell docker images | grep "$(DI_NAME)\s*$(DI_TAG)" | sed -n "s/[0-9a-z\/-]*[[:space:]]*[0-9\.]*[[:space:]]*\([0-9a-z]*\).*/\1/p")
DC_ID := $(shell docker ps -a | grep "$(DI_NAME):$(DI_TAG).*$(DC_NAME)" | sed -n "s/\([0-9a-z]*\).*/\1/p")
DC_STARTED_ID := $(shell docker ps | grep "$(DI_NAME):$(DI_TAG).*$(DC_NAME)" | sed -n "s/\([0-9a-z]*\).*/\1/p")



docker-bash: docker-start
		$(eval DC_ID := $(shell docker ps -a | grep "$(DI_NAME):$(DI_TAG)" | sed -n "s/\([0-9a-z]*\).*/\1/p"))
		docker exec -it $(DC_ID) /bin/bash

docker-start: docker-run
ifeq ($(DC_STARTED_ID),)
		docker start $(DC_NAME)
endif

docker-stop:
		docker stop $(DC_NAME)

docker-run: docker-build
ifeq ($(DC_ID),)
		docker run -d \
			-v $(DIR)/mnt/home/steam/steamcmd:/home/steam/steamcmd/ \
			-p 27015:27015/tcp \
			-p 27015:27015/udp \
			-it --name $(DC_NAME) $(DI_NAME):$(DI_TAG)
endif

docker-build:
ifeq ($(DI_ID),)
		docker build \
			-t $(DI_NAME):$(DI_TAG) ./Docker
endif


docker-clean:
		-docker stop $(DC_NAME)
		-docker rm $(DC_NAME)

docker-clean-all: docker-clean
		docker rmi $(DI_NAME):$(DI_TAG)
